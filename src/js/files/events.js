import { noMoney, deleteMoney, addRemoveClass } from "./functions.js";
import { rotateDrum, getTargetBlock, checkTargetItem } from './script.js';

const preloader = document.querySelector('.preloader');

// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {
	let targetElement = e.target;

	if (targetElement.closest('.header__privacy ') && preloader.classList.contains('_hide')) {
		preloader.classList.remove('_hide');
	}

	if (targetElement.closest('.preloader__button')) {
		preloader.classList.add('_hide');
	}

	if (targetElement.closest('.bet-box__minus')) {
		const bet = +sessionStorage.getItem('current-bet');
		if (bet > 100) {
			sessionStorage.setItem('current-bet', bet - 50);
			document.querySelector('.bet-box__score').textContent = sessionStorage.getItem('current-bet');
		}
	}
	if (targetElement.closest('.bet-box__plus')) {
		const bet = +sessionStorage.getItem('current-bet');
		if (+sessionStorage.getItem('money') > bet + 50) {
			sessionStorage.setItem('current-bet', bet + 50);
			document.querySelector('.bet-box__score').textContent = sessionStorage.getItem('current-bet');
		} else noMoney('.score');
	}

	if (targetElement.closest('.drum__button')) {
		if (+sessionStorage.getItem('money') >= 1000) {
			rotateDrum();
			deleteMoney(1000, '.score', 'money');
			addRemoveClass('.drum__button', '_hold', 2500);

			setTimeout(() => {
				let block = getTargetBlock();
				checkTargetItem(block);
			}, 2100);
		}

	}
	if (targetElement.closest('.win__btn')) {
		document.querySelector('.win').classList.remove('_active');
	}
})